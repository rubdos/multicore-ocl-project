use image::{ImageBuffer, Luma};
use ocl::{self, Buffer, Image, ProQue};
use ocl::enums::{ImageChannelOrder, ImageChannelDataType, MemObjectType};

use utils::compute_required_space;

// I have two sequential implementations:
// One in Rust, almost literally translated from the Python one,
// one actually in OpenCL

// Implementation in Rust:
pub fn expand(s: &[u8], output: &mut Vec<u8>) {
    for c in s.iter() {
        match c {
            b'X' => output.extend_from_slice(b"X+YF+"),
            b'Y' => output.extend_from_slice(b"-FX-Y"),
            x => output.push(*x),
        }
    }
}

pub fn expand_n_rust(mut input: Vec<u8>, n: usize) -> Vec<u8> {
    let len = compute_required_space(&input, n);
    input.reserve(len);
    let mut output = Vec::with_capacity(len);
    for i in 0..n {
        if i % 2 == 0 {
            output.clear();
            expand(&input, &mut output);
        } else {
            input.clear();
            expand(&output, &mut input);
        };
    }
    if n % 2 == 0 {
        input
    } else {
        output
    }
}

// OpenCL:

simple_ocl_source!(Dragon, DragonSource, "sequential.cl");

impl Dragon {
    pub fn expand_n(&self, source: &[u8], n: usize) -> ocl::Result<Vec<u8>> {
        // Compute the correct amount of memory
        let len = compute_required_space(source, n);

        let mut s = vec![0u8; len];
        s.split_at_mut(source.len()).0.copy_from_slice(source);

        let source_buffer = self.pq.buffer_builder()
            // .flags(MemFlags::new().read_write())
            .len(len)
            .copy_host_slice(&s)
            .build()?;


        let result_buffer: Buffer<u8> = self.pq.buffer_builder()
            .len(len)
            .build()?;
        let mut result_vec = vec![0; len];
        result_buffer.write(&result_vec).enq()?;

        let kern = self.pq.kernel_builder("sequential_expand_n")
            .arg(source.len() as u64)
            .arg(n as u64)
            .arg(&source_buffer)
            .arg(&result_buffer)
            .build()?;

        unsafe{ kern.enq()?; }

        result_buffer.read(&mut result_vec).enq()?;

        self.pq.queue().finish()?;

        Ok(result_vec)
    }

    pub fn expand_to_image(&self, source: &[u8], n: usize) -> ocl::Result<ImageBuffer<Luma<u8>, Vec<u8>>> {
        // Compute the correct amount of memory
        let len = compute_required_space(source, n);

        let mut s = vec![0u8; len];
        s.split_at_mut(source.len()).0.copy_from_slice(source);

        let source_buffer = self.pq.buffer_builder()
            // .flags(MemFlags::new().read_write())
            .len(len)
            .copy_host_slice(&s)
            .build()?;

        let result_buffer: Buffer<u8> = self.pq.buffer_builder()
            .len(len)
            .build()?;
        let mut result_vec = vec![0; len];
        result_buffer.write(&result_vec).enq()?;

        let img_dim = 2u32.pow(1+(n/2) as u32) * 5; // 5 pix per seg

        let mut img = ImageBuffer::<Luma<u8>, _>::new(img_dim, 2*img_dim/3);
        let image = Image::<u8>::builder()
	        .channel_order(ImageChannelOrder::Luminance)
	        .channel_data_type(ImageChannelDataType::UnormInt8)
	        .image_type(MemObjectType::Image2d)
            .dims(img.dimensions())
            .flags(ocl::flags::MEM_WRITE_ONLY | ocl::flags::MEM_HOST_READ_ONLY | ocl::flags::MEM_COPY_HOST_PTR)
            .copy_host_slice(&img)
	        .queue(self.pq.queue().clone())
	        .build()?;

        let kern_image = self.pq.kernel_builder("sequential_image")
            .arg(source.len() as u64)
            .arg(n as u64)
            .arg(&source_buffer)
            .arg(&result_buffer)
            .arg(&image)
            .build()?;

        unsafe{ kern_image.enq()?; }

        result_buffer.read(&mut result_vec).enq()?;
        image.read(&mut img).enq()?;

        self.pq.queue().finish()?;

        Ok(img)
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;
    use utils::OclProgramCombinatorBuilder;

    use super::*;

    #[test]
    fn test_expansion_rust() {
        let seed = b"FX".to_vec();
        let mut result = vec![];
        expand(&seed, &mut result);
        assert_eq!(b"FX+YF+".to_vec(), result);

        assert_eq!(b"FX+YF++-FX-YF+".to_vec(), expand_n_rust(seed, 2));
    }

    #[test]
    fn test_expansion_cl() {
        let seed = b"FX".to_vec();

        let dragon = DragonSource.build_program()
            .expect("Compile dragon");

        for i in 1..20 {
            let expanded_rust = expand_n_rust(seed.clone(), i);
            let expanded = dragon.expand_n(&seed, i).expect("expand dragon");
            assert_eq!(expanded_rust, expanded);
        }
    }

    #[test]
    fn test_expansion_to_image_cl() {
        let seed = b"FX".to_vec();

        let dragon = DragonSource.build_program()
            .expect("Compile dragon");

        for i in 1..20 {
            let img = dragon.expand_to_image(&seed, i).expect("expand dragon");

            if i == 19 {
                img.save(&Path::new("dragon-20-seq.png")).unwrap();
            }
        }
    }
}
