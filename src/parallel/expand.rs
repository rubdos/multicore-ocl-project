use ocl::{self, Buffer, Kernel, ProQue};

use super::prefixsum::*;

simple_ocl_source!(Dragon, DragonSource, "dragon.cl");

impl Dragon {
    pub fn build_state(&self, prefixsum: PrefixSum,
                       input: &[u8],
                       max_n: usize)
        -> ocl::Result<DragonState<LSystemState>>
    {
        self.from_string(prefixsum, input, max_n)
    }

    fn from_string(&self,
                   prefixsum: PrefixSum,
                   input: &[u8],
                   max_n: usize)
        -> ocl::Result<DragonState<LSystemState>>
    {
        let size = ::utils::compute_required_space(input, max_n);

        let mut prefixsum_kernel = prefixsum.kernel_u32()?;
        prefixsum_kernel.set_len(input.len());

        let mut input_vec = vec![0u8; size];
        input_vec.split_at_mut(input.len()).0.copy_from_slice(input);
        let dragon_buffer_in: Buffer<u8> = self.pq.buffer_builder()
            .len(size)
            .copy_host_slice(&input_vec)
            .build()?;
        let dragon_buffer_out: Buffer<u8> = self.pq.buffer_builder()
            .len(size)
            .build()?;

        let cost_buffer: Buffer<u32> = self.pq.buffer_builder()
            .len(size)
            .build()?;
        let index_buffer: Buffer<u32> = self.pq.buffer_builder()
            .len(size)
            .build()?;

        let dragon_kernel = self.pq.kernel_builder("expand_dragon")
            .global_work_size(input.len())
            .arg(&dragon_buffer_in)
            .arg(&dragon_buffer_out)
            .arg(&cost_buffer)
            .arg(&index_buffer)
            .build()?;

        let expansion_rate = input.iter()
            .filter(|a| **a == b'X' || **a == b'Y')
            .count();

        let mut state = DragonState {
            _state: LSystemState,
            dragon_buffer_in,
            dragon_buffer_out,
            cost_buffer,
            index_buffer,
            dragon_kernel,
            prefixsum_kernel,
            current_iteration: 0,
            source_length: input.len(),
            expansion_rate,
        };
        state.compute_cost(input)?;
        Ok(state)
    }

    pub fn expand_n(&self,
                    prefixsum: PrefixSum,
                    input: &[u8],
                    count: usize)
        -> ocl::Result<Vec<u8>>
    {
        let mut state = self.build_state(prefixsum, input, count)?;
        for _ in 0..count {
            state = state
                .compute_indices()?
                .compute_dragon()?;
        }
        state.to_string()
    }

    /// Only does a single expansion
    pub fn expand(&self, prefixsum: PrefixSum, input: &[u8])
        -> ocl::Result<Vec<u8>>
    {
        let state = self.build_state(prefixsum, input, 1)?;
        let state = state
            .compute_indices()?
            .compute_dragon()?;
        state.to_string()
    }
}

pub struct DragonState<S> {
    _state: S,
    dragon_buffer_in: Buffer<u8>,
    dragon_buffer_out: Buffer<u8>,
    cost_buffer: Buffer<u32>,
    index_buffer: Buffer<u32>,
    dragon_kernel: Kernel,
    prefixsum_kernel: PrefixSumKernel<u32>,
    current_iteration: usize,
    source_length: usize,
    expansion_rate: usize,
}

pub struct LSystemState;

impl DragonState<LSystemState> {
    pub fn compute_indices(self)
        -> ocl::Result<DragonState<CostState>>
    {
        // Unpack everything
        let DragonState {
            dragon_buffer_in,
            dragon_buffer_out,
            mut cost_buffer,
            mut index_buffer,
            dragon_kernel,
            mut prefixsum_kernel,
            current_iteration,
            source_length,
            expansion_rate,
            ..
        } = self;

        let size = 4*((1<<current_iteration) - 1) * expansion_rate
            + source_length;
        prefixsum_kernel.set_len(size);

        let (c,i) = prefixsum_kernel.compute(cost_buffer, index_buffer)?;
        cost_buffer = c;
        index_buffer = i;

        Ok(DragonState {
            _state: CostState,
            dragon_buffer_in,
            dragon_buffer_out,
            cost_buffer,
            index_buffer,
            dragon_kernel,
            prefixsum_kernel,
            current_iteration,
            source_length,
            expansion_rate,
        })
    }

    fn compute_cost(&mut self, input: &[u8]) -> ocl::Result<()> {
        let mut cost = vec![0u32; self.cost_buffer.len()];
        for (i, character) in input.iter().enumerate() {
            cost[i] = match character {
                b'F' | b'-' | b'+' => 0,
                b'X' | b'Y' => 4,
                _ => unreachable!(),
            };
        }
        self.cost_buffer.write(&cost).enq()?;
        Ok(())
    }

    /// Warning: will not alter maximum length!
    pub fn reset(mut self, input: &[u8]) -> ocl::Result<Self> {
        let mut input_vec = vec![0u8; self.dragon_buffer_in.len()];
        input_vec.split_at_mut(input.len()).0.copy_from_slice(input);

        self.current_iteration = 0;
        self.source_length = input.len();

        self.dragon_buffer_in.write(&input_vec).enq()?;
        self.compute_cost(input)?;
        self.expansion_rate = input.iter()
            .filter(|a| **a == b'X' || **a == b'Y')
            .count();

        Ok(self)
    }

    pub fn to_string(&self) -> ocl::Result<Vec<u8>> {
        let mut out = vec![0u8; self.dragon_buffer_out.len()];
        self.dragon_buffer_in.read(&mut out).enq()?;
        Ok(out)
    }

    pub fn as_string(self) -> Buffer<u8> {
        self.dragon_buffer_in
    }

    pub fn iterate(self) -> ocl::Result<Self> {
        self.compute_indices()?
            .compute_dragon()
    }
}

pub struct CostState;

impl DragonState<CostState> {
    #[cfg(test)]
    fn read_offsets(&self) -> ocl::Result<Vec<u32>> {
        let mut costs = vec![0; self.index_buffer.len()];
        self.index_buffer.read(&mut costs).enq()?;
        Ok(costs)
    }

    pub fn compute_dragon(self)
        -> ocl::Result<DragonState<LSystemState>>
    {
        // Unpack everything
        let DragonState {
            dragon_buffer_in,
            dragon_buffer_out,
            cost_buffer,
            index_buffer,
            mut dragon_kernel,
            prefixsum_kernel,
            mut current_iteration,
            source_length,
            expansion_rate,
            ..
        } = self;

        let size = 4*((1<<current_iteration) - 1) * expansion_rate
            + source_length;
        dragon_kernel.set_default_global_work_size(size.into());
        dragon_kernel.set_arg(0, &dragon_buffer_in)?;
        dragon_kernel.set_arg(1, &dragon_buffer_out)?;
        dragon_kernel.set_arg(2, &cost_buffer)?;
        dragon_kernel.set_arg(3, &index_buffer)?;
        unsafe{ dragon_kernel.enq()? };

        current_iteration += 1;

        Ok(DragonState {
            _state: LSystemState,
            dragon_buffer_in: dragon_buffer_out,
            dragon_buffer_out: dragon_buffer_in,
            cost_buffer,
            index_buffer,
            dragon_kernel,
            prefixsum_kernel,
            current_iteration,
            source_length,
            expansion_rate,
        })
    }
}

#[cfg(test)]
mod tests {
    use utils::*;
    use super::*;

    use ::sequential::{expand, expand_n_rust};

    #[cfg(feature = "cpuprofiler")]
    use cpuprofiler::PROFILER;

    #[test]
    fn test_compute_cost() {
        let seed = b"FX+YF+";

        let mut result = vec![];
        expand(seed, &mut result);

        let (dragon, prefixsum) = (DragonSource, PrefixSumSource)
            .build_program()
            .expect("building dragon.cl or prefixsum");

        let dragon = dragon.build_state(prefixsum, seed, 0)
            .expect("build initial state")
            .compute_indices()
            .expect("Compute costs");

        let costs = dragon.read_offsets()
            .expect("reading offsets");

        assert_eq!(costs, vec![0, 4, 4, 8, 8, 8]);
    }

    #[test]
    fn test_single_expand() {
        let seed = b"FX";

        let mut result = vec![];
        expand(seed, &mut result);

        let (dragon, prefixsum) = (DragonSource, PrefixSumSource)
            .build_program()
            .expect("building dragon.cl or prefixsum");

        let dragon = dragon.expand(prefixsum, seed)
            .expect("Single expansion");

        assert_eq!(dragon, result);
    }

    #[test]
    fn test_multiple_expand() {
        let seed = b"FX";
        let count = 4;

        let reference = expand_n_rust(seed.to_vec(), count);

        let (dragon, prefixsum) = (DragonSource, PrefixSumSource)
            .build_program()
            .expect("building dragon.cl or prefixsum");

        let mut state = dragon.build_state(prefixsum, seed, count)
            .expect("Build state");

        #[cfg(feature = "cpuprofiler")]
        PROFILER.lock().unwrap().start("dragon_expand_profile").unwrap();
        for _ in 0..count {
            state = state.iterate().expect("single iteration");
        }
        #[cfg(feature = "cpuprofiler")]
        PROFILER.lock().unwrap().stop().unwrap();
        let dragon = state.to_string()
            .expect("Read back");

        assert_eq!(dragon, reference);
    }

    #[test]
    fn test_expand_after_reset() {
        let seed = b"FX";
        let count = 6;

        let reference = expand_n_rust(seed.to_vec(), count);

        let (dragon, prefixsum) = (DragonSource, PrefixSumSource)
            .build_program()
            .expect("building dragon.cl or prefixsum");

        let mut state = dragon.build_state(prefixsum, seed, count)
            .expect("Build state");

        for _ in 0..count {
            state = state.iterate().expect("single iteration");
        }
        let dragon = state.to_string()
            .expect("Read back");

        assert_eq!(dragon, reference);

        state = state.reset(seed).unwrap();

        for _ in 0..count {
            state = state.iterate().expect("single iteration");
        }
        let dragon = state.to_string()
            .expect("Read back");

        assert_eq!(dragon, reference);
    }
}

