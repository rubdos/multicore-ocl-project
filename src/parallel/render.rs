use image::{ImageBuffer, Luma};
use ocl::{self, Buffer, Image, Kernel, ProQue};
use ocl::enums::{ImageChannelOrder, ImageChannelDataType, MemObjectType};

use super::prefixsum::*;

simple_ocl_source!(Render, RenderSource, "render.cl");

impl Render {
    pub fn build_state_from_slice(&self,
                                  dragon: &[u8],
                                  img_dim: u32,
                                  prefixsum: PrefixSum)
        -> ocl::Result<ComputeRotationState>
    {
        let dragon = self.pq.buffer_builder()
            .len(dragon.len())
            .copy_host_slice(dragon)
            .build()?;
        self.build_state(dragon, img_dim, prefixsum)
    }

    pub fn build_state(&self,
                       dragon: Buffer<u8>,
                       img_dim: u32,
                       prefixsum: PrefixSum)
        -> ocl::Result<ComputeRotationState>
    {
        let rotations = self.pq.buffer_builder()
            .len(dragon.len())
            .build()?;

        let directions = self.pq.buffer_builder()
            .len(dragon.len())
            .build()?;

        let vectors = self.pq.buffer_builder()
            .len(2 * (dragon.len() + 1))
            .build()?;

        let positions = self.pq.buffer_builder()
            .len(2 * (dragon.len() + 1))
            .build()?;
        let image_buffer = ImageBuffer::new(img_dim, 2*img_dim/3);
        let image = Image::<u8>::builder()
	        .channel_order(ImageChannelOrder::Luminance)
	        .channel_data_type(ImageChannelDataType::UnormInt8)
	        .image_type(MemObjectType::Image2d)
            .dims((img_dim, 2*img_dim/3))
            .flags(ocl::flags::MEM_WRITE_ONLY
                   | ocl::flags::MEM_HOST_READ_ONLY
                   | ocl::flags::MEM_COPY_HOST_PTR)
            .copy_host_slice(&image_buffer)
	        .queue(self.pq.queue().clone())
	        .build()?;

        let compute_vectors = self.pq.kernel_builder("compute_vectors")
            .global_work_size(dragon.len())
            .arg(&dragon)
            .arg(&directions)
            .arg(&vectors)
            .build()?;
        let compute_rotations = self.pq.kernel_builder("compute_rotations")
            .global_work_size(dragon.len())
            .arg(&dragon)
            .arg(&rotations)
            .build()?;
        let render_kernel = self.pq.kernel_builder("render")
            .global_work_size(dragon.len())
            .arg(&positions)
            .arg(&image)
            .build()?;

        Ok(ComputeRotationState {
            pq: self.pq.clone(),
            vector_kernel: prefixsum.kernel_vector()?,
            direction_kernel: prefixsum.kernel_direction()?,
            compute_vectors,
            compute_rotations,
            render_kernel,
            image_buffer,
            dragon, rotations, directions, vectors, positions, image,
        })
    }
}

pub struct ComputeRotationState {
    pq: ProQue,
    vector_kernel: PrefixSumKernel<Vector>,
    direction_kernel: PrefixSumKernel<Direction>,
    compute_rotations: Kernel,
    compute_vectors: Kernel,
    render_kernel: Kernel,
    dragon: Buffer<u8>,
    rotations: Buffer<u8>,
    directions: Buffer<u8>,
    vectors: Buffer<i32>,
    positions: Buffer<i32>,
    image: Image<u8>,
    image_buffer: ImageBuffer<Luma<u8>, Vec<u8>>,
}

impl ComputeRotationState {
    pub fn compute_directions(self)
        -> ocl::Result<ComputeVectorState>
    {
        let ComputeRotationState {
            pq,
            vector_kernel,
            mut direction_kernel,
            compute_rotations,
            compute_vectors,
            render_kernel,
            image_buffer,
            dragon, rotations, mut directions, vectors, positions, image,
        } = self;

        // FX+YF+
        // 001001
        unsafe{ compute_rotations.enq()? };

        // 001001
        // 001112
        direction_kernel.set_len(rotations.len());
        let (_, d) = direction_kernel.compute(rotations, directions)?;
        directions = d;

        Ok(ComputeVectorState {
            pq,
            vector_kernel,
            compute_vectors,
            render_kernel,
            image_buffer,
            dragon, directions, vectors, positions, image,
        })
    }
}

pub struct ComputeVectorState {
    pq: ProQue,
    vector_kernel: PrefixSumKernel<Vector>,
    compute_vectors: Kernel,
    render_kernel: Kernel,
    dragon: Buffer<u8>,
    directions: Buffer<u8>,
    vectors: Buffer<i32>,
    positions: Buffer<i32>,
    image: Image<u8>,
    image_buffer: ImageBuffer<Luma<u8>, Vec<u8>>,
}

impl ComputeVectorState {
    #[cfg(test)]
    pub fn read_directions(&self) -> ocl::Result<Vec<u8>> {
        let mut res = vec![0u8; self.directions.len()];
        self.directions.read(&mut res).enq()?;
        Ok(res)
    }

    pub fn compute_positions(self) -> ocl::Result<ComputeImageState> {
        let ComputeVectorState {
            pq,
            mut vector_kernel,
            compute_vectors,
            render_kernel,
            image_buffer,
            dragon, directions, vectors, mut positions, image,
        } = self;

        unsafe {compute_vectors.enq()?}
        vector_kernel.set_len(vectors.len()/2);
        positions = vector_kernel.compute(vectors, positions)?.1;

        drop(directions);
        drop(dragon);

        Ok(ComputeImageState {
            pq,
            render_kernel,
            positions,
            image,
            image_buffer,
        })
    }
}

pub struct ComputeImageState {
    pq: ProQue,
    render_kernel: Kernel,
    positions: Buffer<i32>,
    image: Image<u8>,
    image_buffer: ImageBuffer<Luma<u8>, Vec<u8>>,
}

impl ComputeImageState {
    #[cfg(test)]
    fn read_positions(&self) -> ocl::Result<Vec<(i32, i32)>> {
        let mut res = vec![0; self.positions.len()];

        self.positions.read(&mut res).enq()?;

        Ok(res.chunks(2).map(|v| {
            (v[0], v[1])
        }).collect())
    }

    pub fn compute_image(self)
        -> ocl::Result<ImageBuffer<Luma<u8>, Vec<u8>>>
    {
        let ComputeImageState {
            pq,
            render_kernel,
            positions,
            image,
            mut image_buffer,
        } = self;

        render_kernel.set_arg(0, &positions)?;

        unsafe{ render_kernel.enq()?; }

        pq.finish()?;

        drop(positions);

        image.read(&mut image_buffer).enq()?;

        Ok(image_buffer)
    }
}

#[cfg(test)]
mod tests {
    use utils::*;
    use super::*;

    #[test]
    fn test_directions() {
        let tests = [
            (b"FX+YF+".to_vec(), vec![0, 0, 1, 1, 1, 2]),
            (b"-F-".to_vec(), vec![3, 3, 2]),
            (b"+F-".to_vec(), vec![1, 1, 0]),
            (b"++++".to_vec(), vec![1, 2, 3, 0]),
        ];
        let (render, prefixsum) = (RenderSource, PrefixSumSource).build_program()
            .expect("Build render.cl source");

        for (dragon, response) in tests.iter() {
            let process = render.build_state_from_slice(dragon, 10, prefixsum.clone())
                .expect("Bind kernels");

            let directions = process.compute_directions()
                .expect("computing rotations or directions");
            let directions = directions.read_directions()
                .expect("Could not read result");
            assert_eq!(directions, *response);
        }
    }

    #[test]
    fn test_vectors() {
        let tests = [
            (b"++++".to_vec(), vec![(0, 0), (0, 0), (0, 0), (0, 0), (0, 0)]),
            (b"FX+YF+".to_vec(), vec![(0, 0), (1, 0), (1, 0), (1, 0), (1, 0), (1, -1), (1, -1)]),
            (b"-F-".to_vec(), vec![(0, 0), (0, 0), (0, 1), (0, 1)]),
            (b"+F-".to_vec(), vec![(0, 0), (0, 0), (0, -1), (0, -1)]),
        ];
        let (render, prefixsum) = (RenderSource, PrefixSumSource).build_program()
            .expect("Build render.cl source");

        for (dragon, response) in tests.iter() {
            let process = render.build_state_from_slice(dragon, 10, prefixsum.clone())
                .expect("Bind kernels");

            let directions = process.compute_directions()
                .expect("computing rotations or directions");
            let vectors = directions.compute_positions()
                .expect("computing vectors or positions");
            let positions = vectors.read_positions()
                .expect("reading positions");

            assert_eq!(positions, *response);
        }
    }
}
