use std;
use ocl::{self, Buffer, Kernel, ProQue};

use utils::log2ceil;

simple_ocl_source!(PrefixSum, PrefixSumSource, "prefix-sum.cl");

pub struct Vector;
pub struct Direction;

impl PrefixSum {
    pub fn kernel_i32(&self) -> ocl::Result<PrefixSumKernel<i32>> {
        PrefixSumKernel::<i32>::new(self.pq.clone())
    }

    pub fn kernel_u32(&self) -> ocl::Result<PrefixSumKernel<u32>> {
        PrefixSumKernel::<u32>::new(self.pq.clone())
    }

    pub fn kernel_vector(&self)
        -> ocl::Result<PrefixSumKernel<Vector>>
    {
        PrefixSumKernel::<Vector>::new(self.pq.clone())
    }

    pub fn kernel_direction(&self)
        -> ocl::Result<PrefixSumKernel<Direction>>
    {
        PrefixSumKernel::<Direction>::new(self.pq.clone())
    }

    pub fn prepare_buffers(&self, input: &[i32])
        -> ocl::Result<(Buffer<i32>, Buffer<i32>)> {
        let input_buffer: Buffer<i32> = self.pq.buffer_builder()
            .len(input.len())
            .copy_host_slice(&input)
            .build()?;

        let output_buffer: Buffer<i32> = self.pq.buffer_builder()
            .len(input.len())
            .build()?;

        Ok((input_buffer, output_buffer))
    }

    pub fn compute(&self, input: &[i32]) -> ocl::Result<Vec<i32>> {
        let mut out = vec![0i32; input.len()];

        let mut kernel = self.kernel_i32()?;
        kernel.set_len(input.len());

        let (input, output) = self.prepare_buffers(input)?;

        let (_input, output) =
            kernel.compute(input, output)?;
        output.read(&mut out).enq()?;

        Ok(out)
    }
}

pub struct PrefixSumKernel<SumType> {
    step: Kernel,
    _sum_type: std::marker::PhantomData<SumType>,
}

impl<S> PrefixSumKernel<S> {
    pub fn set_len(&mut self, len: usize) {
        self.step.set_default_global_work_size(len.into());
    }
}

macro_rules! impl_sum_kernel {
    ($type:ty, $name:expr) => {
        impl_sum_kernel!($type, $type, $name);
    };
    ($rusttype:ty, $datatype:ty, $name:expr) => {
        impl PrefixSumKernel<$rusttype> {
            fn new(pq: ProQue) -> ocl::Result<Self> {
                let step = pq.kernel_builder($name)
                    .arg(0u32)
                    .arg(0u32)
                    .arg(None::<&Buffer<$datatype>>)
                    .arg(None::<&Buffer<$datatype>>)
                    .build()?;

                Ok(PrefixSumKernel {
                    step,
                    _sum_type: std::marker::PhantomData,
                })
            }

            pub fn compute(&self,
                       mut input: Buffer<$datatype>,
                       mut output: Buffer<$datatype>)
                -> ocl::Result<(Buffer<$datatype>, Buffer<$datatype>)>
            {
                // debug_assert!(input.len() == output.len(),
                //     "Input/output buffers need to be same length");
                // debug_assert!(self.step.default_global_work_size()
                //               == ocl::SpatialDims::One(input.len()),
                //     "Please use set_len() on Kernel before calling compute");

                for i in 0..log2ceil(input.len()) {
                    self.step.set_arg(0, i as u32)?;
                    self.step.set_arg(1, input.len() as u32)?;
                    self.step.set_arg(2, &input)?;
                    self.step.set_arg(3, &output)?;
                    unsafe { self.step.enq()? };

                    let temp = input;
                    input = output;
                    output = temp;
                }
                Ok((output, input))
            }
        }
    };
}

impl_sum_kernel!(i32, "prefix_sum_step_i32");
impl_sum_kernel!(u32, "prefix_sum_step_u32");
impl_sum_kernel!(Vector, i32, "prefix_sum_step_vector");
impl_sum_kernel!(Direction, u8, "prefix_sum_step_direction");

#[cfg(test)]
mod tests {
    use utils::*;
    use super::*;

    #[test]
    fn test_prefix_sum() {
        let prefixsum = PrefixSumSource.build_program()
            .expect("build prefixsum");

        let test_vectors = [
            (vec![0, 0, 0, 0], vec![0, 0, 0, 0i32]),
            (vec![1, 0, 0, 0], vec![1, 1, 1, 1]),
            (vec![3, -3, 0, 0], vec![3, 0, 0, 0]),
            (vec![3, -3, 0, 0, 1], vec![3, 0, 0, 0, 1]),
            (vec![3, -3, 0, 1, 0], vec![3, 0, 0, 1, 1]),
        ];

        for (source, sum) in test_vectors.iter() {
            let computed = prefixsum.compute(source)
                .expect("compute prefixsum");
            assert_eq!(source.len(), computed.len());
            assert_eq!(&computed as &[i32], &sum as &[i32]);
        }
    }
}
