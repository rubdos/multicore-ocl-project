__kernel void expand_dragon(
        __global uchar const *const in,
        __global uchar *const out,
        __global unsigned int *expansion_cost,
        __global unsigned int const *indices) {
    unsigned int i = get_global_id(0);
    unsigned int j = i;
    if (i>0) j += indices[i-1];
    // printf("i: %d; j: %d;\n", i, j);

    switch (in[i]) {
        case 'X':
            out[j] = 'X'; expansion_cost[j++] = 4;
            out[j] = '+'; expansion_cost[j++] = 0;
            out[j] = 'Y'; expansion_cost[j++] = 4;
            out[j] = 'F'; expansion_cost[j++] = 0;
            out[j] = '+'; expansion_cost[j++] = 0;
            break;
        case 'Y':
            out[j] = '-'; expansion_cost[j++] = 0;
            out[j] = 'F'; expansion_cost[j++] = 0;
            out[j] = 'X'; expansion_cost[j++] = 4;
            out[j] = '-'; expansion_cost[j++] = 0;
            out[j] = 'Y'; expansion_cost[j++] = 4;
            break;
        case '\0':
            // Avoid buffer overflows
            break;
        default:
            out[j] = in[i]; expansion_cost[j++] = 0;
            break;
    }
}
