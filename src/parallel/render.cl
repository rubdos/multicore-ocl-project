__kernel void compute_vectors(
        __global uchar const *const dragon,
        __global uchar const *const directions,
        __global int *const vectors) {
    const unsigned int i = get_global_id(0);
    const unsigned int j = 2*(1+i);

    switch(dragon[i]) {
        case 'F':
            switch(directions[i]) {
                case 0:
                    vectors[j] = 1;
                    vectors[j + 1] = 0;
                    break;
                case 1:
                    vectors[j] = 0;
                    vectors[j + 1] = -1;
                    break;
                case 2:
                    vectors[j] = -1;
                    vectors[j + 1] = 0;
                    break;
                case 3:
                    vectors[j] = 0;
                    vectors[j + 1] = 1;
                    break;
                default: // Sum ting wong
                    break;
            }
            break;
        default:
            vectors[j] = 0;
            vectors[j+1] = 0;
            break;
    }

    if(i==0) {
        vectors[0] = 0;
        vectors[1] = 0;
    }
}

__kernel void compute_rotations(
        __global uchar const *const dragon,
        __global uchar *const rotations) {
    unsigned int i = get_global_id(0);

    switch (dragon[i]) {
        case '+':
            rotations[i] = 1;
            break;
        case '-':
            rotations[i] = 3;
            break;
        default:
            rotations[i] = 0;
            break;
    }
}

__kernel void render(
        __global int const *const positions,
        write_only image2d_t image)
{
    unsigned int i = 2*get_global_id(0);
    unsigned int j = i+2;

    int2 dim = get_image_dim(image);

    // We draw a line from positions[i] to positions[j]
    int2 begin = (int2)(positions[i], positions[i+1]);
    int2 end = (int2)(positions[j], positions[j+1]);
    int2 direction = end-begin;
    begin *= 2; // pixels
    for(int k = 0; k < 2; ++k) {
        int2 coord = ((begin + k*direction)%dim + dim) % dim;
        write_imagef(image, coord, (float4)(1, 1, 1, 1));
    }
}
