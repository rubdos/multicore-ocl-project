__kernel void prefix_sum_step_i32(
        unsigned int i,
        unsigned int n,
        __global int *in,
        __global int *out) {
    unsigned int j = get_global_id(0);
    // out[j] = select(in[j] + in[(j - (1<<i)) % n], in[j], j < (1<<i));

    if (j < (1<<i)) {
        out[j] = in[j];
    } else {
        out[j] = in[j] + in[j - (1<<i)];
    }
}

__kernel void prefix_sum_step_u32(
        unsigned int i,
        unsigned int n,
        __global unsigned int *in,
        __global unsigned int *out) {
    unsigned int j = get_global_id(0);
    if (j < (1<<i)) {
        out[j] = in[j];
    } else {
        out[j] = in[j] + in[j - (1<<i)];
    }
}

__kernel void prefix_sum_step_vector(
        unsigned int i,
        unsigned int n,
        __global int *in,
        __global int *out) {
    unsigned int j = get_global_id(0);
    unsigned int k = 2*j;
    if (j < (1<<i)) {
        out[k] = in[k];
        out[k+1] = in[k+1];
    } else {
        out[k] = in[k] + in[2* (j - (1<<i))];
        out[k+1] = in[k+1] + in[2*(j - (1<<i)) + 1];
    }
}

__kernel void prefix_sum_step_direction(
        unsigned int i,
        unsigned int n,
        __global const uchar *const in,
        __global uchar *const out) {
    unsigned int j = get_global_id(0);
    if (j < (1<<i)) {
        out[j] = in[j];
    } else {
        out[j] = in[j] + in[j - (1<<i)];
    }
    out[j] %= 4;
}
