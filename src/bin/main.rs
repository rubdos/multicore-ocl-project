extern crate lsystem;
extern crate ocl;

use std::path::Path;

use lsystem::sequential::expand_n_rust;
use lsystem::parallel::*;
use lsystem::utils::*;

use std::env::args;

fn main() {
    let seed = b"FX";
    let count = args().skip(1).next().unwrap().parse().unwrap();

    let reference = expand_n_rust(seed.to_vec(), count);

    let (platform, device) = ocl::Platform::list().into_iter()
        .map(|p| (p, ocl::Device::list(p.clone(), Some(ocl::flags::DeviceType::GPU)).into_iter().next()))
        .filter(|(_p, d)| d.is_some() && d.as_ref().unwrap().len() > 0)
        .map(|(p,d)| (p, ocl::builders::DeviceSpecifier::Single(d.unwrap()[0])))
        .next()
        .unwrap();

    let (dragon, prefixsum, render) = (DragonSource, PrefixSumSource, RenderSource)
        .build_with_device(platform, device)
        .expect("building dragon.cl or prefixsum");

    let mut state = dragon.build_state(prefixsum.clone(), seed, count)
        .expect("Build state");

    #[cfg(feature = "cpuprofiler")]
    PROFILER.lock().unwrap().start("dragon_expand_profile").unwrap();
    for _ in 0..count {
        state = state.iterate().expect("single iteration");
    }
    #[cfg(feature = "cpuprofiler")]
    PROFILER.lock().unwrap().stop().unwrap();

    debug_assert_eq! {
        state.to_string().expect("Read back"),
        reference
    };

    let img_dim = 2u32.pow((count/2) as u32) * 2; // 5 pix per seg

    let dragon = state.as_string();
    let render = render.build_state(dragon, img_dim, prefixsum)
        .expect("Build render state machine")
        .compute_directions().expect("Computing directions")
        .compute_positions().expect("Computing directions")
        .compute_image().expect("Computing image");

    render.save(&Path::new(&format!("dragon-{}-par.png", count))).unwrap();
}
