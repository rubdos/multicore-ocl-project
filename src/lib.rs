extern crate ocl;
extern crate image;
extern crate num_traits;
#[cfg(feature = "cpuprofiler")]
extern crate cpuprofiler;

#[macro_use]
pub mod utils;

pub mod sequential;
pub mod parallel;
