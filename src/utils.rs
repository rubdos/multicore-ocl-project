use std;

use num_traits;
use ocl::{self, Platform, ProQue};
use ocl::enums::DeviceSpecifier;
use ocl::builders::ProgramBuilder;

pub fn compute_required_space(source: &[u8], n: usize) -> usize {
    let xycount = source.iter()
        .filter(|a| **a == b'X' || **a == b'Y')
        .count();
    // len = \sum_0^(n-1) {4*2^n}
    let len = (1<<n) - 1;
    4 * len * xycount + source.len()
}

pub trait OclProgramSource {
    type Program;

    fn src() -> &'static str;
    fn build(self, ProQue) -> ocl::Result<Self::Program>;
}

pub trait OclProgramCombinatorBuilder {
    type Programs;
    fn build_program(self) -> ocl::Result<Self::Programs>
    where Self: Sized {
        self.build_with_device(ocl::Platform::first()?, 0)
    }
    fn build_with_device<D: Into<DeviceSpecifier>>(self, p: Platform, d: D)
        -> ocl::Result<Self::Programs>;
}

macro_rules! impl_opcb {
    ( $($name:ident)+ ) => {
        impl<$($name : $crate::utils::OclProgramSource),+> OclProgramCombinatorBuilder for ($($name),+) {
            type Programs = ($($name :: Program),+);
            #[allow(non_snake_case)]
            fn build_with_device<Dev>(self, p: Platform, d: Dev)
                -> ocl::Result<Self::Programs>
                where Dev: Into<DeviceSpecifier>
            {
                let mut prog = ProgramBuilder::new();
                $(prog.src($name::src());)*;

                let pq = ProQue::builder()
                    .prog_bldr(prog)
                    .dims(1)
                    .platform(p)
                    .device(d)
                    .build()?;


                let ($($name),*) = self;
                Ok(($($name.build(pq.clone())?),+))
            }
        }
    };
}

impl<T: OclProgramSource> OclProgramCombinatorBuilder for T {
    type Programs = T::Program;

    fn build_with_device<Dev>(self, p: Platform, d: Dev)
        -> ocl::Result<Self::Programs>
            where Dev: Into<DeviceSpecifier>
    {
        let pq = ProQue::builder()
            .src(Self::src())
            .dims(1)
            .platform(p)
            .device(d)
            .build()?;
        self.build(pq)
    }
}

impl_opcb! {A B}
impl_opcb! {A B C}
impl_opcb! {A B C D}
impl_opcb! {A B C D E}

#[macro_export]
macro_rules! simple_ocl_source {
    ($objname:ident, $srcname:ident, $file:expr) => {
        #[derive(Debug,Default)]
        pub struct $srcname;
        #[derive(Clone)]
        pub struct $objname {
            pq: ProQue,
        }
        impl $crate::utils::OclProgramSource for $srcname {
            type Program = $objname;
            fn src() -> &'static str {
                include_str!($file)
            }
            fn build(self, pq: ProQue) -> ocl::Result<$objname> {
                Ok($objname {
                    pq,
                })
            }
        }
    };
}

// Returns ceil(log2(i)) for i > 0
//         -1            for i == 0
pub fn log2ceil<T: num_traits::int::PrimInt + std::fmt::Display>(i: T) -> i32 {
    let bits = 8*std::mem::size_of::<T>() as i32;
    let bitscanreverse = bits - i.leading_zeros() as i32;

    if i & (i-num_traits::identities::one()) != num_traits::identities::zero() {
        bitscanreverse
    } else {
        bitscanreverse - 1
    }
}

#[test]
fn test_log2ceil() {
    let tests = [
        (0, -1),
        (32, 5),
        (64, 6),
    ];

    for (val, log) in tests.iter() {
        assert_eq!(log2ceil(*val), *log);
    }
}

#[test]
fn test_compute_required_space() {
    let tests = [
        (b"FX", 1, 6),
        (b"FX", 2, 14),
        (b"XX", 1, 10),
        (b"XX", 2, 26),
    ];

    for (input, iterations, result) in tests.iter() {
        assert_eq!(compute_required_space(*input, *iterations), *result);
    }
}
