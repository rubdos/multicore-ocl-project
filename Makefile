SEQUENTIAL_RESULTS:=dragon_curve.jpg  fractal_plant.jpg  fractal_tree.jpg  fractal_tree_stochastic.jpg
SEQUENTIAL_FILES:=$(foreach X, $(SEQUENTIAL_RESULTS), sequential/$X)


all: $(SEQUENTIAL_FILES)

sequential/%.jpg: sequential/%.py
	(cd sequential; python2 $*.py)
