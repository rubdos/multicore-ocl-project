#!/bin/bash

echo "Generating Dragon Curve.."
python2 dragon_curve.py

echo "Generating Fractal Plant.."
python2 fractal_plant.py

echo "Generating Fractal Tree.."
python2 fractal_tree.py

echo "Generating Stochastic Fractal Tree.."
python2 fractal_tree_stochastic.py

echo "Done. Generated following outputs:"

ls *.jpg
