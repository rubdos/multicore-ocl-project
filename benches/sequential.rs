#[macro_use]
extern crate criterion;
extern crate lsystem;
extern crate ocl;

use criterion::Criterion;

use lsystem::utils;
use lsystem::utils::OclProgramCombinatorBuilder;

fn sequential_benchmark_rust(c: &mut Criterion) {
    let count = 10;

    c.bench_function(&format!("seq-rust {}", count), move |b| {
        let seed = b"FX".to_vec();
        b.iter(move || lsystem::sequential::expand_n_rust(seed.clone(), count))
    });
}

fn sequential_benchmark_cl(c: &mut Criterion) {
    let seed = b"FX";
    let count = 10;

    for platform in ocl::Platform::list() {
        for device in ocl::Device::list(platform, None).unwrap() {
            let dragon = lsystem::sequential::DragonSource.build_with_device(platform.clone(), device)
                .expect("Cannot create Dragon");

            c.bench_function(&format!("seq-cl {} {}",
                                      count,
                                      device.name().unwrap()),
                             move |b|
                             b.iter(|| {
                                 dragon.expand_n(seed, count)
                                     .expect("CL error")
                             }));
        }
    }
}

fn compute_required_space(c: &mut Criterion) {
    let tests = [
        (b"FX", 1),
        (b"FX", 2),
        (b"FX", 50),
        (b"XX", 1),
        (b"XX", 2),
    ];

    for (input, iterations) in tests.iter().cloned() {
        let name = format!("compute_required_space ({}) {}",
                           String::from_utf8(input.to_vec()).unwrap(),
                           iterations);
        c.bench_function(&name,
                         move |b| b.iter(||
                                         utils::compute_required_space(input, iterations)));
    }
}

criterion_group!(benches,
                 sequential_benchmark_cl,
                 sequential_benchmark_rust,
                 compute_required_space);
criterion_main!(benches);
