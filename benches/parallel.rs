#[macro_use]
extern crate criterion;
extern crate lsystem;
extern crate ocl;
extern crate rand;

use criterion::Criterion;
use rand::Rng;

use lsystem::parallel::*;
use lsystem::utils::OclProgramCombinatorBuilder;

fn full_round(c: &mut Criterion) {
    let seed = b"FX";
    let counts = &[10, 15, 20, 22];
    for count in counts {
        for platform in ocl::Platform::list() {
            for device in ocl::Device::list(platform, None).unwrap() {
                let name = format!("par-full {} {}",
                                   count,
                                   device.name().unwrap());
                let (dragon, prefixsum, render) = (DragonSource, PrefixSumSource, RenderSource)
                    .build_with_device(platform.clone(), device)
                    .expect("building dragon.cl or prefixsum.cl or render.cl");

                let img_dim = 2u32.pow((count/2) as u32) * 2; // 5 pix per seg

                c.bench_function(&name, move |b| {
                    b.iter(|| {
                        let mut state = dragon.build_state(prefixsum.clone(), seed, *count)
                            .expect("build dragon state");

                        for _ in 0..*count {
                            state = state.iterate().expect("Iterating dragon");
                        }
                        let dragon = state.as_string();

                        render.build_state(dragon.clone(), img_dim, prefixsum.clone())
                            .expect("Build render state machine")
                            .compute_directions()
                            .expect("Computing directions")
                            .compute_positions()
                            .expect("Computing directions")
                            .compute_image()
                            .expect("Computing image")
                    })
                });
            }
        }
    }
}

fn render_image(c: &mut Criterion) {
    let seed = b"FX";
    let counts = &[10, 15, 20, 22];
    for count in counts {
        for platform in ocl::Platform::list() {
            for device in ocl::Device::list(platform, None).unwrap() {
                let name = format!("par-render {} {}",
                                   count,
                                   device.name().unwrap());
                let (dragon, prefixsum, render) = (DragonSource, PrefixSumSource, RenderSource)
                    .build_with_device(platform.clone(), device)
                    .expect("building dragon.cl or prefixsum.cl or render.cl");

                let img_dim = 2u32.pow((count/2) as u32) * 2; // 5 pix per seg

                c.bench_function(&name, move |b| {
                    let mut state = dragon.build_state(prefixsum.clone(), seed, *count)
                        .expect("build dragon state");
                    for _ in 0..*count {
                        state = state.iterate().expect("Iterating dragon");
                    }
                    let dragon = state.as_string();

                    b.iter(|| {
                        render.build_state(dragon.clone(), img_dim, prefixsum.clone())
                            .expect("Build render state machine")
                            .compute_directions()
                            .expect("Computing directions")
                            .compute_positions()
                            .expect("Computing directions")
                            .compute_image()
                            .expect("Computing image")
                    })
                });
            }
        }
    }
}

fn dragon_expansion(c: &mut Criterion) {
    let seed = b"FX";
    let counts = &[10, 15, 20, 22];
    for count in counts {
        for platform in ocl::Platform::list() {
            for device in ocl::Device::list(platform, None).unwrap() {
                let name = format!("par-expand {} {}",
                                   count,
                                   device.name().unwrap());
                let (dragon, prefixsum) = (DragonSource, PrefixSumSource)
                    .build_with_device(platform.clone(), device)
                    .expect("building dragon.cl or prefixsum");

                let state = dragon.build_state(prefixsum.clone(), seed, *count)
                    .expect("Build state");

                let mut initial_state = Some(state);

                c.bench_function(&name, move |b| {
                    b.iter(|| {
                        let mut state = initial_state.take()
                            .expect("State was destroyed");
                        state = state
                            .reset(seed)
                            .expect("Reset state iteration");

                        for _ in 0..*count {
                            state = state
                                 .iterate()
                                 .expect("Iteration");
                        }
                        initial_state = Some(state);
                    })
                });
            }
        }
    }
}

fn prefix_sum(c: &mut Criterion) {
    let sizes = (0..).map(|i| 1024 * (1<<i));

    for size in sizes.take(10) {
        for platform in ocl::Platform::list() {
            for device in ocl::Device::list(platform, None).unwrap() {
                macro_rules! bench_for_type {
                    ($t:ident, $name:expr) => {
                        let prefixsum = PrefixSumSource
                            .build_with_device(platform.clone(), device)
                            .expect(&format!("Could not compile prefix sum for {}",
                                             device.name().unwrap()));

                        let name = format!("prefix-sum-cl {} ({}) items on {}",
                                           size,
                                           $name,
                                           device.name().unwrap());

                        c.bench_function(&name, move |b| {
                            let mut input = vec![0i32; size];
                            rand::thread_rng().fill(&mut input[..]);

                            let prefixsum_kernel = prefixsum.$t()
                                .expect("create kernel");
                            let (input, output) = prefixsum
                                .prepare_buffers(&input)
                                .expect("Prepare buffers");

                            b.iter(|| {
                                prefixsum_kernel.compute(input.clone(), output.clone())
                                    .expect("Computation failure");
                            })
                        });
                    };
                }
                bench_for_type!(kernel_i32, "i32");
                bench_for_type!(kernel_vector, "vector");
            }
        }
    }
}

criterion_group!(benches, dragon_expansion, prefix_sum, render_image, full_round);
criterion_main!(benches);
